#ifndef CERES_PERSON_H
#define CERES_PERSON_H

#include <string>

/**
 * \brief An individual who represents an element of the clan, a sub-part of
 *        the community.
 * 
 * Within the Clan, this person is identified by a unique name which can be
 * his real name or his nickname.
*/
class Person
{
    public:
        Person() = delete;
        Person(std::string const & name);
        ~Person() = default;

        std::string const & getName() const;

    private:
        std::string _name;
};

#endif /* CERES_PERSON_H */
