#include "Person.h"

/**
 * \brief  Constructs the Person with its name.
 * 
 * \param  The name, it can be a real name or a nickname.
*/
Person::Person(std::string const & name)
{
    _name = name;
}

/**
 * \brief  Returns the person's name, which can be a real name or a nickname.
 * 
 * \return Its name.
*/
std::string const & Person::getName() const
{
    return _name;
}