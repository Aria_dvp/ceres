##Ceres

Ceres is a project whose purpose is to provide nutritional advice for communities that seek total food autonomy, by giving advice about how to manage crops and vegetable gardens, in order to meet the food needs of each member of the community, and to build configurable reserve stocks to offer more food resilience.

It is developed in modern C ++, and I haven't decided on the license yet.
