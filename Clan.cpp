#include <iostream>

#include "Clan.h"

/**
 * \brief Prints the number of clan members, the number of guests, and their name.
 * 
 * This function is used for debugging purposes, it should not stay in time.
*/
void Clan::printEverybody() const
{
    std::cout << "- Printing " << _guests.size() << " guests :" << std::endl;
    for (auto & person : _guests)
        std::cout << "    - " << person.getName() << std::endl;

    std::cout << "- Printing " << _members.size() << " members :" << std::endl;
    for (auto & person : _members)
        std::cout << "    - " << person.getName() << std::endl;

    std::cout << std::endl; 
}

/**
 * \brief Add a member to the clan, this person will be an official member.
 * 
 * \param person    the person to add as a full official member.
*/
void Clan::addMember(Person && person)
{
    _members.push_back(std::move(person));
}

/**
 * \brief Add a guest to the clan, this person will temporary share the
 *        ressources with the clan.
 * 
 * \param person    the person to add as a guest.
*/
void Clan::addGuest(Person && person)
{
    _guests.push_back(std::move(person));
}

/**
 * \brief Remove the member identified by the name given in parameter.
 * 
 * \param name  the person's name to remove from the clan.
*/
void Clan::removeMember(std::string const & name)
{
    for (auto it = _members.begin(); it != _members.end(); ++it)
    {
        if (it->getName() == name)
        {
            _members.erase(it);
            break;
        }
    }
}

/**
 * \brief Remove the guest identified by the name given in parameter.
 * 
 * \param name  the person's name to remove from the clan.
*/
void Clan::removeGuest(std::string const & name)
{
    for (auto it = _guests.begin(); it != _guests.end(); ++it)
    {
        if (it->getName() == name)
        {
            _guests.erase(it);
            break;
        }
    }
}


/**
 * \brief  Change an actual guest, identified by the name given in parameter,
 *         into a true clan member.
 * 
 * \param  name  the person's name to move from a guest to a clan member.
 * 
 * \return false if there is no guest with this name, true otherwise.
*/
bool Clan::moveGuestIntoMember(std::string const & name)
{
    for (auto it = _members.begin(); it != _members.end(); ++it)
    {
        if (it->getName() == name)
        {
            _guests.push_back(std::move(*it));
            return true;
        }
    }
    return false;
}

/**
 * \brief  Change an actual member, identified by the name given in parameter,
 *         into a simple guest.
 * 
 * \param  name  the person's name to move from a member to a guest.
 * 
 * \return false if there is no clan member with this name, true otherwise.
*/
bool Clan::moveMemberIntoGuest(std::string const & name)
{
    for (auto it = _guests.begin(); it != _guests.end(); ++it)
    {
        if (it->getName() == name)
        {
            _members.push_back(std::move(*it));
            return true;
        }
    }
    return false;
}

/**
 * \brief  Returns a collection of Persons identified as clan members.
 * 
 * \return all the Persons belonging to the clan, except the guests.
*/
People const & Clan::getMembers() const
{
    return _members;
}

/**
 * \brief  Returns a collection of Persons identified as guests.
 * 
 * \return all the Persons belonging to the clan, except the clan members.
*/
People const & Clan::getGuests() const
{
    return _guests;
}


/**
 * \brief  TODO
 * 
 * \todo  
*/
void Clan::getHealth() const
{
    // not done yet
}