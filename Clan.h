#ifndef CERES_CLAN_H
#define CERES_CLAN_H

#include <list>
#include <memory>
#include <string>

#include "Person.h"

using People = std::list<Person>;

/**
 * \brief This is your family, your group of friends, the people you trust
 *  in order to share your ressources and produce food together.
*/
class Clan
{
public:
    Clan() = default;
    ~Clan() = default;

    void printEverybody() const;
    People const & getMembers() const;
    People const & getGuests() const;

    void addMember(Person && person);
    void addGuest(Person && person);
    void removeMember(std::string const & name);
    void removeGuest(std::string const & name);
    bool moveGuestIntoMember(std::string const & name);
    bool moveMemberIntoGuest(std::string const & name);

    void getHealth() const;

private:
    People _members;    
    People _guests;
};

#endif /* CERES_CLAN_H */
