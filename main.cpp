#include <iostream>
#include <memory>
#include <string>

#include "Clan.h"

void printTitle(std::string title)
{
    std::string separator(title.size() + 8, '#');

    std::cout << separator << std::endl;
    std::cout << "### " << title << " ###" << std::endl;
    std::cout << separator << std::endl;
}


int main()
{
    std::cout << "Hello Ceres" << std::endl << std::endl;

    Person trevor("Trevor");
    Person navis("Navis");
    Clan clan;

    clan.addMember(std::move(trevor));
    printTitle("Trevor is the leader");
    clan.printEverybody();

    clan.addGuest(std::move(navis));
    printTitle("Then Navis is a guest");
    clan.printEverybody();

    clan.removeGuest("Navis");
    printTitle("Then Navis die :(");
    clan.printEverybody();

    std::fflush;

    return 0;
}
